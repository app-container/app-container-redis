poky-app-container-redis
=======================

build local container
=====================

```
pushd local_scripts/
```
```
./docker_build.sh
```
```
popd
```

run local container with ash
============================

```
pushd local_scripts/
```

```
./docker_run-ash.sh reslocal/poky-app-container-redis
```

```
...
+ ID=$(docker run -t -i -d -p 6379:6379 reslocal/poky-app-container-redis ash -l)
+ ID 115a69fdf44f6e64af834522c7f1a112350c0e4eea663e023ebd4f1f725fe92c
+ docker attach 115a69fdf44f6e64af834522c7f1a112350c0e4eea663e023ebd4f1f725fe92c
...
```

```
root@115a69fdf44f:/# /etc/init.d/redis-server restart
Stopping redis-server...
Starting redis-server...
```

```
root@115a69fdf44f:/# cat /var/log/redis.log 
```
```
10:C 15 May 13:31:02.039 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
10:C 15 May 13:31:02.039 # Redis version=4.0.14, bits=64, commit=9001e77e, modified=0, pid=10, just started
10:C 15 May 13:31:02.039 # Configuration loaded
                _._                                                  
           _.-``__ ''-._                                             
      _.-``    `.  `_.  ''-._           Redis 4.0.14 (9001e77e/0) 64 bit
  .-`` .-```.  ```\/    _.,_ ''-._                                   
 (    '      ,       .-`  | `,    )     Running in standalone mode
 |`-._`-...-` __...-.``-._|'` _.-'|     Port: 6379
 |    `-._   `._    /     _.-'    |     PID: 11
  `-._    `-._  `-./  _.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |           http://redis.io        
  `-._    `-._`-.__.-'_.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |                                  
  `-._    `-._`-.__.-'_.-'    _.-'                                   
      `-._    `-.__.-'    _.-'                                       
          `-._        _.-'                                           
              `-.__.-'                                               

11:M 15 May 13:31:02.071 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
11:M 15 May 13:31:02.071 # Server initialized
11:M 15 May 13:31:02.071 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
11:M 15 May 13:31:02.071 # WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.
11:M 15 May 13:31:02.071 * Ready to accept connections
root@115a69fdf44f:/# 
```

```
root@115a69fdf44f:/# exit
```

```
popd
```


run without ash
===============

```
pushd local_scripts/
```
```
./docker_run.sh reslocal/poky-app-container-redis
```

```
...
+ ID=$(docker run -t -i -d -p 6379:6379 reslocal/poky-app-container-redis)
+ ID 2afa31b4376b7e5640cf946c8f27fd51580bcec3683980792fc9d4a9a1ceea57
+ docker attach 2afa31b4376b7e5640cf946c8f27fd51580bcec3683980792fc9d4a9a1ceea57
                _._
           _.-``__ ''-._
      _.-``    `.  `_.  ''-._           Redis 4.0.14 (9001e77e/0) 64 bit
  .-`` .-```.  ```\/    _.,_ ''-._
 (    '      ,       .-`  | `,    )     Running in standalone mode
 |`-._`-...-` __...-.``-._|'` _.-'|     Port: 6379
 |    `-._   `._    /     _.-'    |     PID: 10
  `-._    `-._  `-./  _.-'    _.-'
 |`-._`-._    `-.__.-'    _.-'_.-'|
 |    `-._`-._        _.-'_.-'    |           http://redis.io
  `-._    `-._`-.__.-'_.-'    _.-'
 |`-._`-._    `-.__.-'    _.-'_.-'|
 |    `-._`-._        _.-'_.-'    |
  `-._    `-._`-.__.-'_.-'    _.-'
      `-._    `-.__.-'    _.-'
          `-._        _.-'
              `-.__.-'

10:M 15 May 13:42:11.931 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
10:M 15 May 13:42:11.931 # Server initialized
10:M 15 May 13:42:11.931 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
10:M 15 May 13:42:11.931 # WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.
10:M 15 May 13:42:11.931 * Ready to accept connections

```
```
popd
```

kill it
=======
from another shell
```
pushd local_scripts/
```

```
./docker_kill.sh reslocal/poky-app-container-redis
```
```
popd
```

